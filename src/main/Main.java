package main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import controller.Implementations.LoginControllerImpl;
import model.Implementations.CellImpl;
import model.Implementations.GuardImpl;
import model.Implementations.GuardImplBuilder;
import view.Interfaces.LoginView;

/**
 * The main of the application.
 */
public final class Main {

    /**
     * Program main, this is the "root" of the application.
     *
     * @param args
     * unused,ignore
     */


    private static final int FIRST_FLOOR = 20;
    private static final int SECOND_FLOOR = 40;
    private static final int THIRD_FLOOR = 45;
    private static final int TOTAL_FLOOR = 50;

    public static void main(final String... args) {

        String mainDirectory = "res";
        new File(mainDirectory).mkdir();
        File fileGuards = new File("res/GuardsUserPass.txt");

        if (fileGuards.length() == 0) {
            try {
                initializeGuards(fileGuards);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        File fileCells = new File("res/Cells.txt");
        if (fileCells.length() == 0) {
            try {
                initializeCells(fileCells);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        new LoginControllerImpl(new LoginView());
    }

    /**
     * metodo che inizializza le celle
     *
     * @param fileCells file in cui creare le celle
     * @throws IOException
     */
    static void initializeCells(File fileCells) throws IOException {

        List<CellImpl> cellsList = new ArrayList<>();
        CellImpl c;
        for (int i = 0; i < TOTAL_FLOOR; i++) {
            if (i < FIRST_FLOOR) {
                c = new CellImpl(i, "First floor", 4);
            } else if (i < SECOND_FLOOR) {
                c = new CellImpl(i, "Second floor", 3);
            } else if (i < THIRD_FLOOR) {
                c = new CellImpl(i, "Third floor", 4);
            } else {
                c = new CellImpl(i, "Undergorund floor, isolation cells", 1);
            }
            cellsList.add(c);
        }

        FileOutputStream fileOutputStream = new FileOutputStream(fileCells);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.flush();
        fileOutputStream.flush();
        for (CellImpl c1 : cellsList) {
            objectOutputStream.writeObject(c1);
        }
        objectOutputStream.close();
    }

    /**
     * metodo che inizializza le guardie
     *
     * @param guardsFile file in cui creare le guardie
     * @throws IOException
     */
    static void initializeGuards(File guardsFile) throws IOException {

        String pattern = "MM/dd/yyyy";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Date date = null;

        try {
            date = format.parse("01/01/1980");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<GuardImpl> guardsList = new ArrayList<>();
        GuardImpl firstGuard = new GuardImplBuilder().setName("Oronzo").setSurname("Cantani").setBirthDate(date).setRank(1).setTelephoneNumber("0764568").setIdGuardia(1).setPassword("ciao01").createGuardImpl();
        guardsList.add(firstGuard);
        GuardImpl secondGuard = new GuardImplBuilder().setName("Emile").setSurname("Heskey").setBirthDate(date).setRank(2).setTelephoneNumber("456789").setIdGuardia(2).setPassword("asdasd").createGuardImpl();
        guardsList.add(secondGuard);
        GuardImpl thirdGuard = new GuardImplBuilder().setName("Gennaro").setSurname("Alfieri").setBirthDate(date).setRank(3).setTelephoneNumber("0764568").setIdGuardia(3).setPassword("qwerty").createGuardImpl();
        guardsList.add(thirdGuard);
        FileOutputStream fileOutputStream = new FileOutputStream(guardsFile);
        ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
        outputStream.flush();
        fileOutputStream.flush();
        for (GuardImpl guards : guardsList) {
            outputStream.writeObject(guards);
        }
        outputStream.close();
    }

}
